<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
require_once "connection.php";

   $id = $_GET['id'];
   //selecting data associated with this particular id
   $result = mysqli_query($conn, "SELECT * FROM student WHERE id=$id");
   while($row = mysqli_fetch_array($result)){
    $name = $row['name'];
    $email = $row['email'];
    $hobby=$row['hobby']; 
    // to convert the array to string and seprate by comma 
    $hby=explode(",", $hobby); 
    }


// after click on update button then update the data in the database 
   if(isset($_POST['update'])){
     $id=$_POST['id'];
     $name=$_POST["name"];
     $email=$_POST["email"]; 
     $like=$_POST["hobbies"];
     // again conert the string to array, to store the data in student database  
     $ho=implode(",", $like );   
     $result = mysqli_query($conn, "UPDATE student SET name='$name',email='$email', hobby='$ho' WHERE id=$id");
     // after click on the update button header move on the index file
     header('location:index.php'); 
     mysqli_close($conn);
   }
    
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Update Data</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Update Record</h2>
                    </div>
                    <form action="<?php echo htmlspecialchars(basename($_SERVER['REQUEST_URI'])); ?>" method="post">

                        <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="<?php echo $name; ?>">
                            <span class="help-block"><?php echo $name_err;?></span>
                        </div>

                        <!-- Email input field-->
                        <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                            <label>Email</label>
                            <input type="text" name="email" class="form-control" value="<?php echo $email; ?>">
                            <span class="help-block"><?php echo $email_err;?></span>
                        </div>

                        <!-- class input field-->
                         <div class="form-group">
                            <label>Class</label>
                            <select name="class_level" class="form-control"> 
                                <option value="">--Classes--</option> 
                              <?php
                              include 'connection.php';
                                $class_level =isset($class_level)? $class_level:'';
                                $res=mysqli_query($conn,"Select * from stu_class");
                                   while($row=mysqli_fetch_array($res))
                                    { 
                                        echo "<option ";
                                        if($class_level==$row['class_level']){
                                         echo 'selected';
                                        }
                                        echo ">".$row['class_level']."</option>";
                                    }
                              ?> 
                              </select>
                             
                        </div>  

                       
                        <!-- for the input filed of checkbox -->
                        <!-- in array function used for get the checked value -->
                        <div class="form-group">
                            <label>Hobbies</label>
                            <input type="checkbox" name="hobbies[]" value="Reading" <?php if(in_array("Reading", $hby)){ echo "checked"; } ?> >Reading
                            <input type="checkbox" name="hobbies[]" value="Singing" <?php if(in_array("Singing", $hby)){ echo "checked"; } ?>>Singing
                            <input type="checkbox" name="hobbies[]" value="Sports" <?php if(in_array("Sports", $hby)){ echo "checked"; } ?>>Sports
                            <input type="text" name="hobbies[]" placeholder="other choices">
                            
                        </div>  
                       
                        <!-- three button the update.php  file for update,reset and cancel the update -->
                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>
                        <input type="submit" class="btn btn-primary" value="Update" name="update">
                        <a href="index.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

</body>
</html>