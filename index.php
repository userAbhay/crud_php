<?php
include 'connection.php';
// logic for delete nultiple error
if(isset($_POST["delete"])){
    $num=$_POST["num"];
    $a=implode(",", $num); // to fetch the id 
    mysqli_query($conn,"DELETE FROM student WHERE id in ($a)");
}

// sorting the table in database
$orderby="";
if(isset($_GET['sort'])){
    $orderby = ' order by  '.$_GET['sort']. ' ASC';
}else {

}


// for the pagination purpose 
// select only 10 record per page
$limit = 10;  
if (isset($_GET["page"])) {
    $page  = $_GET["page"]; 
    } 
    else{ 
    $page=1;
    };  
$start_from = ($page-1) * $limit;  

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>Student Information</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style type="text/css">
        .wrapper{
            width: 970px;
            margin: 0 auto;
        }
        .page-header h2{
            margin-top: 0;
        }
    
        table tr td:last-child a{
            margin-right: 15px;
        }
        
    </style>
     <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });


     </script>
    <script>
        // for search the data in the database table on the index.php 
        $(document).ready(function(){
            $("#myInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
            });

            $("#checkAl").click(function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });
        });

        $("#button").click(function(){
            if($('input[type=checkbox]:checked').length == 0){
                alert ( "ERROR! Please select at least one checkbox" );
               //valid = false;
            }
            //if($('#check').val().length==0){
            //    $('#adiv').html("please select at least one checkbox");
            //}
        });

          
           

    </script>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header clearfix">
                        <h2 class="pull-left">Student Information</h2>

                        <!-- for the search input filed in the database -->
                        <input id="myInput" type="text" placeholder="Search..">
                        <!--  for add the data into database and it open the form --> 
                        <a href="create.php" class="btn btn-success pull-right">Add</a>
                    </div>
                    <form method="post" action="" name="form1"> 
                    <?php
                    $i=1;
                    require_once "connection.php";
                    $sql = "SELECT * FROM student {$orderby} LIMIT $start_from, $limit";
                    if($result=mysqli_query($conn,$sql)){
                        if(mysqli_num_rows($result)>0){
                            echo "<table class='table table-bordered table-striped' id='stu_table' >";
                            // for the heading in the table on the index.php
                            echo "<thead>";
                                    echo "<tr>";

                                        echo "<th>" ?>
                                        <input type='submit' value='Delete' name='delete' id="button" class='btn btn-danger btn-xs' >

                                        <?php echo "</th>";
                                        
                                        echo "<th><a href='index.php?sort=id'>S. No.</a></th>";
                                        echo "<th><a href='index.php?sort=name'>Name</a></th>";
                                        echo "<th><a href='index.php?sort=email'>Email</a></th>";
                                        echo "<th><a href='index.php?sort=class'>Class</a></th>";
                                        echo "<th><a href='index.php?sort=hobby'>Hobby</a></th>";
                                        echo "<th><a href='index.php?sort=resume'>Resume</a></th>";
                                        echo "<th>Action</th>";
                                    echo "</tr>";
                                echo "</thead>";
                                echo "<tbody id='myTable'>";
                                while($row = mysqli_fetch_array($result)){
                                    // for fetch the database table 
                                    echo "<tr>";
                                    echo "<th>"?><input type="checkbox" name="num[]" id="check" value="<?php echo $row["id"]; ?>"/> <?php  echo "</th>";
                                        echo "<td>" . $i . "</td>";
                                        echo "<td>" . $row['name'] ."</td>";
                                        echo "<td>" . $row['email'] ."</td>";
                                        echo "<td>" . $row['class'] ."</td>";
                                        echo "<td>" . $row['hobby'] ."</td>";
                                        echo "<td><a>" . $row["resume"]."</a></td>";
                                        echo "<td>";
                                            echo "<a href='read.php?id=". $row['id'] ."' title='View Record' data-toggle='tooltip'><span class='glyphicon glyphicon-eye-open'></span></a>";
                                            echo "<a href='update.php?id=". $row['id'] ."' title='Update Record' data-toggle='tooltip'><span class='glyphicon glyphicon-pencil'></span></a>";
                                            echo "<a href='delete.php?id=". $row['id'] ."' title='Delete Record' data-toggle='tooltip'><span class='glyphicon glyphicon-trash'></span></a>";
                                        echo "</td>";
                                    echo "</tr>";
                                    $i++;
                                }
                                echo "</tbody>";                            
                            echo "</table>";

                            // for show the pagination on the index.php file
                            $result_db = mysqli_query($conn,"SELECT COUNT(id) FROM student"); 
                            $row_db = mysqli_fetch_row($result_db);  
                            $total_records = $row_db[0];  

                            // divide the total row in the database by set limit 10 for show only 10 record on the index file
                            $total_pages = ceil($total_records / $limit); 
                            /* echo  $total_pages; */
                            $pagLink = "<ul class='pagination'>";  
                            for ($i=1; $i<=$total_pages; $i++) {
                                $pagLink .= "<li class='page-item'><a class='page-link' href='index.php?page=".$i."'>".$i."</a></li>";   
                            }
                            echo $pagLink . "</ul>";  

                              mysqli_free_result($result);
                        } else{
                            // if file is empty then error message display
                            echo "<p class='lead'><em>No records were found.</em></p>";
                        }
                    } else{
                        echo "ERROR: Could not able to execute $sql. " . mysqli_error($conn);
                    }
                    // closing the connection after fetch the data
                    mysqli_close($conn);
                

                    ?>
                </form>
                </div>
            </div>
        </div>
    </div>


</body>
</html>
