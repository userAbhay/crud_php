<?php
session_start();
// include connection file for establish a connection
require_once "connection.php";
// to show name and email error 
$name = $email= "";
$name_err = $email_err = "";

// check validation when data load on the server 
if($_SERVER["REQUEST_METHOD"] == "POST"){
   // for name validation error
    $input_name = trim($_POST["name"]);
    if(empty($input_name)){
        $name_err = "Please enter a name.";
    } elseif(!filter_var($input_name, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $name_err = "Please enter a valid name.";
    }elseif (strlen($input_name)>100) {
        $name_err="Name must be less than 100 character";
    } 
     else{
        $name = $input_name;
    }


    // for email validation error
    $input_email = trim($_POST["email"]);
    if(empty($input_email)){
        $email_err="Please Enter email id";
    }
    elseif(!filter_var($input_email,FILTER_VALIDATE_EMAIL)){
        $email_err = "invalid email address";     
    } else{
        $email = $input_email;
    }

    $class=$_REQUEST['class_level'];
    
    
  $filename=$_FILES['resume']['name'];
  move_uploaded_file($_FILES['resume']['tmp_name'],"Upload/".$filename);

  $allowed =  array('pdf','docx' ,'txt');
  $filename = $_FILES['resume']['name'];
  $ext = pathinfo($filename, PATHINFO_EXTENSION);
  if(!in_array($ext,$allowed) ) {
    echo 'error';
  }


   
    // to generate 6 digit random number and store in encrypted form
    $password=MD5(rand(100000,900000));

    // for insert the hobby in the database
    $hoby=$_REQUEST['hobbies'];
    $hobby=implode(",", $hoby);


    // for unique email id check condition
    $sql_e = "SELECT * FROM student WHERE email='$email'";
    $res_e = mysqli_query($conn, $sql_e);
    if (mysqli_num_rows($res_e) > 0) {
      $email_err = "Sorry... email already taken";  
    }else{

    // insert query into database php
    $sql="INSERT INTO student SET name='".$name."' , email='".$email."' , class='".$class."', hobby='".$hobby."', resume='".$filename."' ,password='".$password."' ";
    mysqli_query($conn,$sql) or die(mysqli_error($conn));
    header('location:index.php');
    mysqli_close($conn);

    //$_SESSION['message'] = 'Data added successfully';
    
   // exit();
   
   }

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Create DataBase</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
	<style type="text/css">
		.wrapper{
			width: 500px;
			margin: 0 auto;
		}
	</style>
    <script type="text/javascript">
        function validation(){
                var Name=document.forms["form"]["name"];
                var Email=document.forms["form"]["email"];
                var File=document.forms["form"]["resume"];
                var filePath = File.value;
                var allowedExtensions = /(\.pdf|\.docx|\.txt)$/i;
        
                
                // for validate the name
                if(Name.value==""){
                    window.alert("Enter your First name");
                    Name.focus();
                    return false;
                }
                // to check name between 3 and 20
                if(Name.value<3 || Name.value>20){
                    window.alert("name can not be empty");
                    Name.focus();
                    return false;
                }

                // to check input field is empty 
                if(Email.value==""){
                    window.alert("Email should not be empty");
                    Email.focus();
                    return false;
                }

                // email contain with @ 
                if (Email.value.indexOf("@", 0) < 0){ 
                     window.alert("Please enter a valid e-mail address."); 
                     Email.focus(); 
                     return false; 
                } 
                if (Email.value.indexOf(".", 0) < 0){
                    window.alert("Please enter a valid e-mail address."); 
                    Email.focus(); 
                    return false; 
                }

                if(document.form.class.selectedIndex==""){
                    alert ( "Please select your class!");
                    return false;
                }

                if($("input[name='hobbies[]']:checked").length == 0){
                    alert("Please select hobby");
                    return false;
                }

               

                // to allow only pdf,docx and txt file
                if(!allowedExtensions.exec(filePath)){
                    alert('Please upload file having extensions .pdf/.docx/.txt only.');
                    File.value = '';
                    return false;
                }                
                return true;


            }       
    </script>
</head>
<body>

	<div class="wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="page-header">
						<h2>Create Record</h2>
					</div>
					<form method="post" name="form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" enctype="multipart/form-data" onsubmit="return validation()">

                        <!-- name input field-->
						<div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
							<label>Name</label>
							<input type="text" name="name" class="form-control" value="<?php echo $name; ?>">
							<span class="help-block"><?php echo $name_err; ?></span>
						</div>


                        <!-- Email input field-->
						<div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                            <label>Email</label>
                            <input type="text" name="email" class="form-control" value="<?php echo $email; ?>">
                            <span class="help-block"><?php echo $email_err;?></span>
                        </div>

                        <!-- class input field-->
                         <div class="form-group">
                            <label>Class</label>
                            <select name="class_level" class="form-control" id="class"> 
                                <option value="">--Classes--</option> 
                              <?php
                              include 'connection.php';
                                $res=mysqli_query($conn,"Select * from stu_class");
                                   while($row=mysqli_fetch_array($res))
                                    { 
                                        echo "<option>".$row['class_level']."</option>";
                                    }
                              ?> 
                              </select>
                             
                        </div>  

                        <!-- Hobbies input field-->
                        <div class="form-group <?php echo (!empty($hobby_err)) ? 'has-error' : ''; ?>">
                            <label>Hobbies</label>
                            <input type="checkbox" name="hobbies[]" value="Reading" id="Reading">Reading
                            <input type="checkbox" name="hobbies[]" value="Singing" id="Singing">Singing
                            <input type="checkbox" name="hobbies[]" value="Sports" id="Sports">Sports
                            <input type="checkbox" name="hobbies[]" placeholder="other choices">Others
                            
                        </div>

                        <!-- Resume input field-->
                         <div class="form-group">
                            <label>Resume</label>
                            <input type="file" name="resume" value="" accept=".txt,.pdf,.docx">
                            <p><strong>Note:</strong> Only .pdf, .docx, .txt formats allowed to a max size of 2 MB.</p>
                        </div>


                        <input type="reset" class="btn btn-primary" value="RESET">
                        <input type="submit" class="btn btn-primary" value="Submit"> 
                        <a href="index.php" class="btn btn-default">Cancel</a>
						
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
