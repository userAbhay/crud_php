-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 13, 2019 at 12:34 AM
-- Server version: 5.6.35-1+deb.sury.org~xenial+0.1
-- PHP Version: 7.2.21-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php`
--

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(225) NOT NULL,
  `class` varchar(255) NOT NULL,
  `hobby` varchar(255) NOT NULL,
  `resume` varchar(255) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `email`, `class`, `hobby`, `resume`, `password`) VALUES
(272, 'gdhfhg', 'abhay@gmail.com', '3', 'Reading', 'fend-syllabus-9.0.0.pdf', '108c18a9f7'),
(273, 'abhay', 'xascf453s@gmail.com', '5', 'Reading', 'Training Topics.docx', '363005d5c2'),
(274, 'abhay', 'abhay23123@gmail.com', '2', 'Reading', 'Training Topics.docx', '7417e039ee'),
(275, 'abhay', 'weqeqwe@gmail.com', '2', 'Reading', 'Training Topics.docx', '29c04f525a'),
(276, 'gdhfhg', 'xasdsfsecfs@gmail.com', '3', 'Reading', 'Training Topics.docx', 'c659361ca0'),
(277, 'abhay', 'xascfdsfss@gmail.com', '2', 'Reading', 'Training Topics.docx', 'cd78d20062'),
(278, 'abhay', 'xassdascfs@gmail.com', '3', 'Reading', 'javascript_test.docx', '3a3deb0881');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=279;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
